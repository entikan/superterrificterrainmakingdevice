from direct.stdpy import thread
from random import choice, randint
from panda3d.core import NodePath
from panda3d.core import Vec2
from panda3d.core import PNMImage, PNMPainter, PNMBrush
from panda3d.core import Texture
from panda3d.core import ShaderTerrainMesh, Shader


UINT = 65535
TERRAIN_SHADER = Shader.load(Shader.SL_GLSL, 'heightmap.vert', 'heightmap.frag')

def empty_image(res, default_value=0):
    img = PNMImage(res, res, num_channels=1, maxval=UINT)
    if not default_value == 0: img.fill(default_value)
    return img

def get_blend_function(image, blend):
    return {
        'add':    image.add_sub_image,
        'darken': image.darken_sub_image,
        'mult':   image.mult_sub_image,
        'copy':   image.copy_sub_image
    }[blend]

def get_processing_function(image, process):
    return {
        'fill':   image.fill,
        'blur':   image.gaussian_filter,
    }[blend]


class PerlinLayer():
    def __init__(self, scale, strength, blend, seed_offset=0, hsv=(1,1,1)):
        self.scale = scale
        self.strength = strength
        self.blend = blend
        self.seed_offset = 0
        self.material_layers = 0
        self.hsv = (1,1,1)


thread_todo = {'mesh':[],'texture':[],'reparent':[]}


class TerrainChunk():
    def __init__(self, terrain, pos, res, height, seed):
        self.root = NodePath('chunk_'+str(pos))
        self.terrain = terrain
        self.pos = pos
        self.res = res
        self.height = height
        self.seed = seed

        self.image  = empty_image(self.res)
        self.image_customized = empty_image(self.res)
        self.image_generated  = empty_image(self.res)
        self.noise = empty_image(self.res)

        self.texture = Texture('chunk_{}-{}'.format(pos.x, pos.y))
        self.painter = PNMPainter(self.image_customized)
        self.painter.pen = PNMBrush.make_spot((1,1,1,0.1),32,True)
        self.built = False

    def refresh(self):
        self.root.detach_node()
        self.make_mesh()

    def make_texture(self):
        self.apply_layers()
        self.image.copy_sub_image(self.image_generated, 0, 0)
        self.image.add_sub_image((self.image_customized*0.5), 0, 0)
        self.texture.load(self.image)
        self.mesh.generate() # HACK: expensive but fixes the borders for now

    def make_mesh(self):
        self.mesh = ShaderTerrainMesh()
        self.mesh.set_heightfield(self.texture)
        self.mesh.set_target_triangle_width(int(self.res/2))
        self.mesh.set_chunk_size(int(self.res/4))
        self.mesh.set_update_enabled(True)
        self.make_texture()
        self.root = self.root.attach_new_node(self.mesh)
        self.root.set_shader(TERRAIN_SHADER)
        self.root.set_shader_input("camera", base.camera)
        self.root.set_scale(self.res, self.res, self.height)
        offset = self.res / 2.0 - 0.5
        x, y = -offset+(self.pos.x*self.res), -offset-(self.pos.y*self.res)
        self.root.set_pos(x, y, -self.height/2)
        #self.mesh.generate()
        self.built = True
        thread_todo['reparent'].append(self.root)

    def apply_layers(self, spread=2):
        self.image_generated.fill(0)
        table_size = int(self.res//2)
        for l, layer in enumerate(self.terrain.layers):
            xscale = (layer.scale.x/self.res*spread)
            yscale = (layer.scale.y/self.res*spread)
            self.noise.perlin_noise_fill(
                xscale, yscale, table_size=table_size, seed=self.seed+layer.seed_offset,
                ox=int(self.pos.x*self.res),oy=int(self.pos.y*self.res)
            )
            #TODO noise post-processing

            blend_func = get_blend_function(self.image_generated, layer.blend)
            blend_func(self.noise, 0, 0, pixel_scale=layer.strength)



class InfiniteTerrain():
    def __init__(self, res, seed=randint(0,99999)):
        self.res = res
        self.seed = seed
        self.height = 256
        self.chunks = {}
        self.radius = 16

        self.layers = []

        self.build_chunks()
        self.thread = thread.start_new_thread(self.thread_do, args=[])
        self.stop_thread = False
        base.task_mgr.add(self.parent_chunks)

    def thread_do(self):
        while True:
            if len(thread_todo['texture']) > 0:
                chunk = thread_todo['texture'][0]
                if not chunk.built: # if it exists but it's not built, do that now instead.
                    thread_todo['mesh'].remove(chunk)
                    chunk.make_mesh()
                else:
                    chunk.make_texture()
                    thread_todo['texture'].pop(0)
            elif len(thread_todo['mesh']) > 0:
                thread_todo['mesh'].pop(0).make_mesh()
            if self.stop_thread:
                return

    def parent_chunks(self, task):
        for to_reparent in thread_todo['reparent']:
            to_reparent.reparent_to(render)
        return task.cont

    def spiral_func(self, func, x=0, y=0):
        X = Y = self.radius
        dx, dy = 0, -1
        for i in range(max(X, Y)**2):
            if (-X/2 < x <= X/2) and (-Y/2 < y <= Y/2):
                func(x,y)
            if x == y or (x < 0 and x == -y) or (x > 0 and x == 1-y):
                dx, dy = -dy, dx
            x, y = x+dx, y+dy

    def add_chunk(self, x, y):
        self.chunks[x,y] = TerrainChunk(self, Vec2(x,y), self.res, self.height, self.seed)
        self.refresh_mesh(x,y)

    def refresh_texture(self, x, y):
        thread_todo['texture'].append(self.chunks[x,y])

    def refresh_mesh(self, x, y):
        thread_todo['mesh'].append(self.chunks[x,y])

    def build_chunks(self):
        self.spiral_func(self.add_chunk)

    def remove_perlin_layer(self, n):
        self.layers.remove(self.layers[n])
        self.spiral_func(self.refresh_texture)

    def add_perlin_layers(self, *layers):
        for layer in layers:
            self.layers.append(layer)
        thread_todo['texture'] = [self.chunks[0,0]]
        self.spiral_func(self.refresh_texture)

    def add_perlin_layers_later(self, time, *layers):
        base.task_mgr.do_method_later(time, self.add_perlin_layers, extraArgs=[*layers], name='add more layers')

if __name__ == '__main__':
    import sys
    from direct.showbase.ShowBase import ShowBase
    from direct.gui.DirectGui import *
    from panda3d.core import PStatClient


    PStatClient.connect()

    base = ShowBase()
    base.set_frame_rate_meter(True)
    terrain = InfiniteTerrain(256)
    #FIXME: when size is lower than 512, there needs to be at least one PerlinLayer on stack
    # (probably because it's doing something before it's supposed to in a thread)

    def close():
        terrain.stop_thread = True
        sys.exit()
    base.accept('escape', close)

    terrain.add_perlin_layers(
        PerlinLayer(Vec2(63,65), 0.5, 'add'),
        PerlinLayer(Vec2(200,200), 0.5, 'add'),
        PerlinLayer(Vec2(800,800), 0.9, 'mult'),
        PerlinLayer(Vec2(65,63), 1.2, 'mult'),
        PerlinLayer(Vec2(32,32), 0.8, 'darken'),
        PerlinLayer(Vec2(16,16), 0.1, 'add'),
        PerlinLayer(Vec2(92,91), 1.2, 'darken'),
        PerlinLayer(Vec2(80,80), 0.8, 'mult'),
        PerlinLayer(Vec2(122,121), 1.5, 'darken'),
        PerlinLayer(Vec2(32,32), 0.1, 'add'),
        PerlinLayer(Vec2(512,513), 1.6, 'darken'),
    )

    base.cam.set_pos(256*8,256*8,256*4)
    base.cam.look_at((0,0,0))
    base.run()

    '''
    def test_paint(task):
        x,y = randint(0,self.res), randint(0,self.res)
        self.chunks[self.selected_chunk].painter.draw_point(x,y)
        self.chunks[self.selected_chunk].make_texture()
        return task.cont
    '''


